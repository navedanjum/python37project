#! /usr/bin/env python3

"""
Created on Jan 27, 2018
@author: Navedanjum Ansari
"""

import tkinter as tk
from tkinter import Menu
from tkinter import ttk

def _quit():
    print("close the tkinter window")
    win.quit()      # win will exist when this function is called
    win.destroy()
    exit()

#create tk instance
win = tk.Tk()

#Add title
win.title("Learning Python GUI")

#Create menu bar instance
menu_bar = Menu()

#Configure tk instance to use menu-bar
win.config(menu = menu_bar)

#Add menu items
file_menu = Menu(menu_bar, tearoff=0)
file_menu.add_command(label="New")

#This puts a line separator between File items New and Exit in rows
file_menu.add_separator()
file_menu.add_command(label="Exit", command=_quit)  #execute _quit function

#File menu has two items New and Exit
menu_bar.add_cascade(label="File", menu = file_menu)

#############################################################################################
# Creating tabs, in tkinter, it is created using  ttk  NoteBook
##############################################################################################

#create tab controls on tkinter window instance
tab_controls = ttk.Notebook(win)

# create tabs
tab1 = ttk.Frame(tab_controls)
#Add the tab1 to tab_control
tab_controls.add(tab1, text = "Tab 1")

tab2 = ttk.Frame(tab_controls)
tab_controls.add(tab2, text = "Tab 2")

tab3 = ttk.Frame(tab_controls)            # Add a second tab
tab_controls.add(tab3, text='Tab 3')      # Make second tab visible

#Make the menu visible using pack
tab_controls.pack(expand=True, fill="both")

#Add new frame
weather_cities_frame = ttk.LabelFrame(tab1, text="Latest Observation for:")
weather_cities_frame.grid(column=0, row=0,padx=8, pady=4)

#Create container frame to hold all other widgets i.e elements using Label frame
weather_condition_frame = ttk.LabelFrame(tab1, text="Current weather conditions")

#tkinter grid layout manager
weather_condition_frame.grid(column=0, row=1, padx=8, pady=4)

# but frame won't be visible until we add widgets to it...
# Adding a Label
# ttk.Label(weather_cities_frame, text="Location:").grid(column=0, row=0, sticky='E') # Stick to (W)est boundary

ttk.Label(weather_cities_frame, text="Weather Station ID: ").grid(column=0, row=0)

# Create combobox(drop down menu)
# city = tk.StringVar()
# city_selected = ttk.Combobox(weather_cities_frame, width=12, textvariable=city)
# city_selected['values'] = ('Los Angeles', 'London', 'Rio de Janeiro', 'Brazil')
# city_selected.current(0) # Highlight first city
#
# #Add to the grid
# city_selected.grid(column=1, row=0)

# ---------------------------------------------------------------
station_id = tk.StringVar()
station_id_combo = ttk.Combobox(weather_cities_frame, width=6, textvariable=station_id)
                        # Los Angeles, Denver, New York City
station_id_combo['values'] = ('KLAX', 'KDEN', 'KNYC')
station_id_combo.grid(column=1, row=0)
station_id_combo.current(0)                 # highlight first city station id

# increase combobox width to longest displayed city string
max_width  = max([len(x)for x in station_id_combo['values']])
new_width = max_width
new_width = max_width + 4          # adjust per taste of extra spacing

#configure city grid to use the new width
station_id_combo.config(width=new_width)

#=================================
ENTRY_WIDTH = 23             # adjust per taste of alignment
#=================================
# Adding Label & Textbox Entry widgets -row-wise entry
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Last Updated:").grid(column=0, row=1, sticky='E')
updated = tk.StringVar()
updated_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=updated, state='readonly')
updated_entry.grid(column=1, row=1, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Weather:").grid(column=0, row=2, sticky='E') # <== increment row for each
weather = tk.StringVar()
weather_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=weather, state='readonly')
weather_entry.grid(column=1, row=2, sticky='W')                                  # <== increment row for each
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Temperature:").grid(column=0, row=3, sticky='E')
temp = tk.StringVar()
temp_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=temp, state='readonly')
temp_entry.grid(column=1, row=3, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Dewpoint:").grid(column=0, row=4, sticky='E')
dew = tk.StringVar()
dew_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=dew, state='readonly')
dew_entry.grid(column=1, row=4, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Relative Humidity:").grid(column=0, row=5, sticky='E')
rel_humi = tk.StringVar()
rel_humi_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=rel_humi, state='readonly')
rel_humi_entry.grid(column=1, row=5, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Wind:").grid(column=0, row=6, sticky='E')
wind = tk.StringVar()
wind_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=wind, state='readonly')
wind_entry.grid(column=1, row=6, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Visibility:").grid(column=0, row=7, sticky='E')
visi = tk.StringVar()
visi_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=visi, state='readonly')
visi_entry.grid(column=1, row=7, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="MSL Pressure:").grid(column=0, row=8, sticky='E')
msl = tk.StringVar()
msl_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=msl, state='readonly')
msl_entry.grid(column=1, row=8, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Altimeter:").grid(column=0, row=9, sticky='E')
alti = tk.StringVar()
alti_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=alti, state='readonly')
alti_entry.grid(column=1, row=9, sticky='W')
#---------------------------------------------

##Add x,y spacing between the elements/widgets fields on frame
# ****************************************************************
# Add some space around each label
for child in weather_condition_frame.winfo_children():
        child.grid_configure(padx=4, pady=2)


#########################################################################################
# NOAA DATA (National Oceanic and Atmospheric Administration) section starts here
#########################################################################################
# callback function
def _get_station():
    get_weather_data()
    populate_gui_from_dict()

get_weather_btn = ttk.Button(weather_cities_frame,text='Get Weather', command=_get_station).grid(column=2, row=0)

# Station City label
# This will be displayed in the cities frame
location = tk.StringVar()
ttk.Label(weather_cities_frame, textvariable=location).grid(column=0, row=1, columnspan=3)

# Spacing adjustment for label and combobox two children of weather_cities_frame
for child in weather_cities_frame.winfo_children():
        print(child)
        child.grid_configure(padx=10, pady=6)


#########################################################################################
# NOAA DATA directly from live web search

#Retrieve the tags we are interested in
weather_data_tags_dict = {
    'observation_time': '',
    'weather': '',
    'temp_f':  '',
    'temp_c':  '',
    'dewpoint_f': '',
    'dewpoint_c': '',
    'relative_humidity': '',
    'wind_string':   '',
    'visibility_mi': '',
    'pressure_string': '',
    'pressure_in': '',
    'location': ''
    }

# ---------------------------------------------------------------
import urllib.request
import xml.etree.ElementTree as ET   # For reading from xml tags

def get_weather_data(station_id='KLAX'):
    url_general = 'http://www.weather.gov/xml/current_obs/{}.xml'
    url = url_general.format(station_id)
    print(url)
    request = urllib.request.urlopen(url)
    #read xml content
    content = request.read().decode()
    #print(content)
    xml_root = ET.fromstring (content)
    print(xml_root)
    print ('xml_root: {}\n'.format (xml_root.tag))

    #Populate dictionary values by reading text from the xml tags
    for data_point in weather_data_tags_dict.keys():
        weather_data_tags_dict[data_point] = xml_root.find(data_point).text

# ---------------------------------------------------------------
def populate_gui_from_dict():
    location.set(weather_data_tags_dict['location'])
    updated.set(weather_data_tags_dict['observation_time'].replace('Last Updated on ', ''))
    weather.set(weather_data_tags_dict['weather'])
    temp.set('{} \xb0F  ({} \xb0C)'.format(weather_data_tags_dict['temp_f'], weather_data_tags_dict['temp_c']))
    dew.set('{} \xb0F  ({} \xb0C)'.format(weather_data_tags_dict['dewpoint_f'], weather_data_tags_dict['dewpoint_c']))
    rel_humi.set(weather_data_tags_dict['relative_humidity'] + ' %')
    wind.set(weather_data_tags_dict['wind_string'])
    visi.set(weather_data_tags_dict['visibility_mi'] + ' miles')
    msl.set(weather_data_tags_dict['pressure_string'])
    alti.set(weather_data_tags_dict['pressure_in'] + ' in Hg')

#########################################################################################
# TAB 2  -- To retrieve stations in a state
#########################################################################################
# We are creating a container frame to hold all other widgets
weather_states_frame = ttk.LabelFrame(tab2, text=' Weather Station IDs ')
weather_states_frame.grid(column=0, row=0, padx=8, pady=4)
# ---------------------------------------------------------------
# Adding a Label
ttk.Label(weather_states_frame, text="Select a State: ").grid(column=0, row=0) # empty space for alignment
# ---------------------------------------------------------------
state = tk.StringVar()
state_combo = ttk.Combobox(weather_states_frame, width=5, textvariable=state)
state_combo['values'] = ('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI',
                         'ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI',
                         'MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC',
                         'ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT',
                         'VT','VA','WA','WV','WI','WY'
                        )
state_combo.grid(column=1, row=0)
state_combo.current(0)                 # highlight first

# ---------------------------------------------------------------
# callback function
def _get_cities():
    state = state_combo.get()
    get_city_station_ids(state)

get_cities_btn = ttk.Button(weather_states_frame,text='Get Cities', command=_get_cities).grid(column=2, row=0)

from tkinter import scrolledtext
scr = scrolledtext.ScrolledText(weather_states_frame, width=30, height=17, wrap=tk.WORD)
scr.grid(column=0, row=1, columnspan=3)

# ---------------------------------------------------------------
for child in weather_states_frame.winfo_children():
        child.grid_configure(padx=6, pady=6)

# ---------------------------------------------------------------
def get_city_station_ids(state='ca'):
    url_general = 'http://w1.weather.gov/xml/current_obs/seek.php?state={}&Find=Find'
    state = state.lower()                       # has to be in lower case
    url = url_general.format(state)
    request = urllib.request.urlopen( url )
    content = request.read().decode()
    #print(content)    #content is in html format

    parser = WeatherHTMLParser()
    parser.feed(content)

    # verify we have as many stations as cities
    print (len (parser.stations) == len (parser.cities))
    scr.delete ('1.0', tk.END)  # clear scrolledText widget for next btn click

    for idx in range(len(parser.stations)):
        city_station = parser.cities[idx] + ' (' + parser.stations[idx] + ')'
        print(city_station)
        scr.insert(tk.INSERT, city_station + '\n')


from html.parser import HTMLParser

class WeatherHTMLParser (HTMLParser):
    def __init__(self):
        super ().__init__ ()
        self.stations = []
        self.cities = []
        self.grab_data = False

    def handle_starttag(self, tag, attrs):
        for attr in attrs:
            if "display.php?stid=" in str (attr):
                cleaned_attr = str (attr).replace ("('href', 'display.php?stid=", '').replace ("')", '')
                self.stations.append (cleaned_attr)
                self.grab_data = True

    def handle_data(self, data):
        if self.grab_data:
            self.cities.append (data)
            self.grab_data = False

#########################################################################################
# TAB 3
#########################################################################################
# We are creating a container frame to hold all other widgets
weather_images_frame = ttk.LabelFrame(tab3, text=' Weather Images ')
weather_images_frame.grid(column=0, row=0, padx=8, pady=4)

# ---------------------------------------------------------------
# requires Pillow (PIL in Python 2.x)
import PIL.Image
import PIL.ImageTk

im = PIL.Image.open("./pics/few_clouds.png")
photo = PIL.ImageTk.PhotoImage(im)
ttk.Label(weather_images_frame, image=photo).grid(column=0, row=0)

im = PIL.Image.open("./pics/night_few_clouds.png")
photo1 = PIL.ImageTk.PhotoImage(im)
ttk.Label(weather_images_frame, image=photo1).grid(column=1, row=0)

im = PIL.Image.open("./pics/night_fair.png")
photo2 = PIL.ImageTk.PhotoImage(im)
ttk.Label(weather_images_frame, image=photo2).grid(column=2, row=0)

#Start GUI
win.mainloop()