#! /usr/bin/env python3

"""
Created on Jan 26, 2018
@author: Navedanjum Ansari
"""

import tkinter as tk
from tkinter import Menu
from tkinter import ttk

def _quit():
    print("close the tkinter window")
    win.quit()      # win will exist when this function is called
    win.destroy()
    exit()

#create tk instance
win = tk.Tk()

#Add title
win.title("Learning Python GUI")

#Create menu bar instance
menu_bar = Menu()

#Configure tk instance to use menu-bar
win.config(menu = menu_bar)

#Add menu items
file_menu = Menu(menu_bar, tearoff=0)
file_menu.add_command(label="New")

#This puts a line separator between File items New and Exit in rows
file_menu.add_separator()
file_menu.add_command(label="Exit", command=_quit)  #execute _quit function

#File menu has two items New and Exit
menu_bar.add_cascade(label="File", menu = file_menu)

#############################################################################################
# Creating tabs, in tkinter, it is created using  ttk  NoteBook
##############################################################################################

#create tab controls on tkinter window instance
tab_controls = ttk.Notebook(win)

# create tabs
tab1 = ttk.Frame(tab_controls)
#Add the tab1 to tab_control
tab_controls.add(tab1, text = "Tab 1")

tab2 = ttk.Frame(tab_controls)
tab_controls.add(tab2, text = "Tab 2")

#Make the menu visible using pack
tab_controls.pack(expand=True, fill="both")

#Add new frame
weather_cities_frame = ttk.LabelFrame(tab1, text="Latest Observation for:")
weather_cities_frame.grid(column=0, row=0,padx=8, pady=4)

#Create container frame to hold all other widgets i.e elements using Label frame
weather_condition_frame = ttk.LabelFrame(tab1, text="Current weather conditions")

#tkinter grid layout manager
weather_condition_frame.grid(column=0, row=1, padx=8, pady=4)

# but frame won't be visible until we add widgets to it...
# Adding a Label
ttk.Label(weather_cities_frame, text="Location:").grid(column=0, row=0, sticky='E') # Stick to (W)est boundary

# Create combobox(drop down menu)
city = tk.StringVar()
city_selected = ttk.Combobox(weather_cities_frame, width=12, textvariable=city)
city_selected['values'] = ('Los Angeles', 'London', 'Rio de Janeiro', 'Brazil')
city_selected.current(0) # Highlight first city

#Add to the grid
city_selected.grid(column=1, row=0)

# Spacing adjustment for label and combobox two children of weather_cities_frame
for child in weather_cities_frame.winfo_children():
        print(child)
        child.grid_configure(padx=10, pady=6)

# increase combobox width to longest displayed city string
max_width  = max([len(x)for x in city_selected['values']])
new_width = max_width
new_width = max_width + 4          # adjust per taste of extra spacing

#configure city grid to use the new width
city_selected.config(width=new_width)

#=================================
ENTRY_WIDTH = 23             # adjust per taste of alignment
#=================================
# Adding Label & Textbox Entry widgets -row-wise entry
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Last Updated:").grid(column=0, row=1, sticky='E')
updated = tk.StringVar()
updated_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=updated, state='readonly')
updated_entry.grid(column=1, row=1, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Weather:").grid(column=0, row=2, sticky='E') # <== increment row for each
weather = tk.StringVar()
weather_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=weather, state='readonly')
weather_entry.grid(column=1, row=2, sticky='W')                                  # <== increment row for each
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Temperature:").grid(column=0, row=3, sticky='E')
temp = tk.StringVar()
temp_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=temp, state='readonly')
temp_entry.grid(column=1, row=3, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Dewpoint:").grid(column=0, row=4, sticky='E')
dew = tk.StringVar()
dew_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=dew, state='readonly')
dew_entry.grid(column=1, row=4, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Relative Humidity:").grid(column=0, row=5, sticky='E')
rel_humi = tk.StringVar()
rel_humi_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=rel_humi, state='readonly')
rel_humi_entry.grid(column=1, row=5, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Wind:").grid(column=0, row=6, sticky='E')
wind = tk.StringVar()
wind_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=wind, state='readonly')
wind_entry.grid(column=1, row=6, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Visibility:").grid(column=0, row=7, sticky='E')
visi = tk.StringVar()
visi_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=visi, state='readonly')
visi_entry.grid(column=1, row=7, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="MSL Pressure:").grid(column=0, row=8, sticky='E')
msl = tk.StringVar()
msl_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=msl, state='readonly')
msl_entry.grid(column=1, row=8, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Altimeter:").grid(column=0, row=9, sticky='E')
alti = tk.StringVar()
alti_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=alti, state='readonly')
alti_entry.grid(column=1, row=9, sticky='W')
#---------------------------------------------

##Add x,y spacing between the elements/widgets fields on frame
# ****************************************************************
# Add some space around each label
for child in weather_condition_frame.winfo_children():
        child.grid_configure(padx=4, pady=2)


#########################################################################################
# NOAA DATA (National Oceanic and Atmospheric Administration) section starts here
# dict data below is a result from web search
# NOAA DATA looks like in below format
# Sample data
#########################################################################################
weather_data = {
'dewpoint_c': '16.7',
 'dewpoint_f': '62.1',
 'dewpoint_string': '62.1 F (16.7 C)',
 'icon_url_base': 'http://forecast.weather.gov/images/wtf/small/',
 'icon_url_name': 'nsct.png',
 'latitude': '33.93806',
 'location': 'Los Angeles, Los Angeles International Airport, CA',
 'longitude': '-118.38889',
 'ob_url': 'http://www.weather.gov/data/METAR/KLAX.1.txt',
 'observation_time': 'Last Updated on Aug 7 2018, 6:53 pm PDT',
 'observation_time_rfc822': 'Sun, 07 Aug 2018 22:53:00 -0700',
 'pressure_in': '29.81',
 'pressure_mb': '1009.1',
 'pressure_string': '1009.1 mb',
 'relative_humidity': '84',
 'station_id': 'KLAX',
 'suggested_pickup': '15 minutes after the hour',
 'suggested_pickup_period': '60',
 'temp_c': '19.4',
 'temp_f': '67.0',
 'temperature_string': '67.0 F (19.4 C)',
 'two_day_history_url': 'http://www.weather.gov/data/obhistory/KLAX.html',
 'visibility_mi': '9.00',
 'weather': 'Partly Cloudy',
 'wind_degrees': '250',
 'wind_dir': 'West',
 'wind_mph': '6.9',
 'wind_string': 'West at 6.9 MPH (6 KT)'
 }

# we want to populate our GUI and we start by using above dict
updated_data = weather_data['observation_time'].replace('Last Updated on ', '')
# next update the Entry widget with this data
updated.set(updated_data)

weather.set(weather_data['weather'])
temp.set('{} \xb0F  ({} \xb0C)'.format(weather_data['temp_f'], weather_data['temp_c']))
#xbo represents the degree symbol for temperatures
dew.set('{} \xb0F  ({} \xb0C)'.format(weather_data['dewpoint_f'], weather_data['dewpoint_c']))
rel_humi.set(weather_data['relative_humidity'] + ' %')
wind.set(weather_data['wind_string'])
visi.set(weather_data['visibility_mi'] + ' miles')
msl.set(weather_data['pressure_string'])
alti.set(weather_data['pressure_in'] + ' in Hg')

#Start GUI
win.mainloop()