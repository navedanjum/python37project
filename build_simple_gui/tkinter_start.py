#! /usr/bin/env python3

"""
Created on Jan 26, 2018
@author: Navedanjum Ansari
"""

import tkinter as tk

# Create instance
win = tk.Tk()

# Add a title
win.title("Learn Python GUI")

def get_current_windows_size():
    win.update()                        # to get runtime size
    print('width  =', win.winfo_width())
    print('height =', win.winfo_height())

def increase_window_width():
    # min width can not be resized to less
    # default heigth can be
    win.minsize(width=300, height=1)    # 1 = default
    #Disable resizing of GUI
    win.resizable(0,0)

# Start GUI
get_current_windows_size()
increase_window_width()
print()
get_current_windows_size()

win.mainloop()