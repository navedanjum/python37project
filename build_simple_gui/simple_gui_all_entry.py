#! /usr/bin/env python3

"""
Created on Jan 26, 2018
@author: Navedanjum Ansari
"""

import tkinter as tk
from tkinter import Menu
from tkinter import ttk

def _quit():
    print("close the tkinter window")
    win.quit()      # win will exist when this function is called
    win.destroy()
    exit()

#create tk instance
win = tk.Tk()

#Add title
win.title("Learning Python GUI")

#Create menu bar instance
menu_bar = Menu()

#Configure tk instance to use menu-bar
win.config(menu = menu_bar)

#Add menu items
file_menu = Menu(menu_bar, tearoff=0)
file_menu.add_command(label="New")

#This puts a line separator between File items New and Exit in rows
file_menu.add_separator()
file_menu.add_command(label="Exit", command=_quit)  #execute _quit function

#File menu has two items New and Exit
menu_bar.add_cascade(label="File", menu = file_menu)

#############################################################################################
# Creating tabs, in tkinter, it is created using  ttk  NoteBook
##############################################################################################

#create tab controls on tkinter window instance
tab_controls = ttk.Notebook(win)

# create tabs
tab1 = ttk.Frame(tab_controls)
#Add the tab1 to tab_control
tab_controls.add(tab1, text = "Tab 1")

tab2 = ttk.Frame(tab_controls)
tab_controls.add(tab2, text = "Tab 2")

#Make the menu visible using pack
tab_controls.pack(expand=True, fill="both")

#Add new frame
weather_cities_frame = ttk.LabelFrame(tab1, text="Latest Observation for:")
weather_cities_frame.grid(column=0, row=0,padx=8, pady=4)

#Create container frame to hold all other widgets i.e elements using Label frame
weather_condition_frame = ttk.LabelFrame(tab1, text="Current weather conditions")

#tkinter grid layout manager
weather_condition_frame.grid(column=0, row=1, padx=8, pady=4)

# but frame won't be visible until we add widgets to it...
# Adding a Label
ttk.Label(weather_cities_frame, text="Location:").grid(column=0, row=0, sticky='E') # Stick to (W)est boundary

# Create combobox(drop down menu)
city = tk.StringVar()
city_selected = ttk.Combobox(weather_cities_frame, width=12, textvariable=city)
city_selected['values'] = ('Los Angeles', 'London', 'Rio de Janeiro', 'Brazil')
city_selected.current(0) # Highlight first city

#Add to the grid
city_selected.grid(column=1, row=0)

# increase combobox width to longest displayed city string
max_width  = max([len(x)for x in city_selected['values']])
new_width = max_width
new_width = max_width            # adjust per taste of extra spacing

#configure city grid to use the new width
city_selected.config(width=new_width)

#==========================
ENTRY_WIDTH = max_width + 3             # adjust per taste of alignment
#==========================
# Adding Label & Textbox Entry widgets -row-wise entry
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Last Updated:").grid(column=0, row=1, sticky='E')
updated = tk.StringVar()
updated_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=updated, state='readonly')
updated_entry.grid(column=1, row=1, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Weather:").grid(column=0, row=2, sticky='E') # <== increment row for each
weather = tk.StringVar()
weather_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=weather, state='readonly')
weather_entry.grid(column=1, row=2, sticky='W')                                  # <== increment row for each
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Temperature:").grid(column=0, row=3, sticky='E')
temp = tk.StringVar()
temp_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=temp, state='readonly')
temp_entry.grid(column=1, row=3, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Dewpoint:").grid(column=0, row=4, sticky='E')
dew = tk.StringVar()
dew_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=dew, state='readonly')
dew_entry.grid(column=1, row=4, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Relative Humidity:").grid(column=0, row=5, sticky='E')
rel_humi = tk.StringVar()
rel_humi_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=rel_humi, state='readonly')
rel_humi_entry.grid(column=1, row=5, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Wind:").grid(column=0, row=6, sticky='E')
wind = tk.StringVar()
wind_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=wind, state='readonly')
wind_entry.grid(column=1, row=6, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Visibility:").grid(column=0, row=7, sticky='E')
visi = tk.StringVar()
visi_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=visi, state='readonly')
visi_entry.grid(column=1, row=7, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="MSL Pressure:").grid(column=0, row=8, sticky='E')
msl = tk.StringVar()
msl_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=msl, state='readonly')
msl_entry.grid(column=1, row=8, sticky='W')
#---------------------------------------------
ttk.Label(weather_condition_frame, text="Altimeter:").grid(column=0, row=9, sticky='E')
alti = tk.StringVar()
alti_entry = ttk.Entry(weather_condition_frame, width=ENTRY_WIDTH, textvariable=alti, state='readonly')
alti_entry.grid(column=1, row=9, sticky='W')
#---------------------------------------------

##Add x,y spacing between the elements/widgets fields on frame
# ****************************************************************
# Add some space around each label
for child in weather_condition_frame.winfo_children():
        child.grid_configure(padx=4, pady=2)


#Start GUI
win.mainloop()