#! /usr/bin/env python3

"""
Created on Jan 26, 2018
@author: Navedanjum Ansari
"""

import tkinter as tk
from tkinter import Menu
from tkinter import ttk

def _quit():
    print("close the tkinter window")
    win.quit()      # win will exist when this function is called
    win.destroy()
    exit()

#create tk instance
win = tk.Tk()

#Add title
win.title("Learning Python GUI")

#Create menu bar instance
menu_bar = Menu()

#Configure tk instance to use menu-bar
win.config(menu = menu_bar)

#Add menu items
file_menu = Menu(menu_bar, tearoff=0)
file_menu.add_command(label="New")

#This puts a line separator between File items New and Exit in rows
file_menu.add_separator()
file_menu.add_command(label="Exit", command=_quit)  #execute _quit function

#File menu has two items New and Exit
menu_bar.add_cascade(label="File", menu = file_menu)

#############################################################################################
# Creating tabs, in tkinter, it is created using  ttk  NoteBook
##############################################################################################

#create tab controls on tkinter window instance
tab_controls = ttk.Notebook(win)

# create tabs
tab1 = ttk.Frame(tab_controls)
#Add the tab1 to tab_control
tab_controls.add(tab1, text = "Tab 1")

tab2 = ttk.Frame(tab_controls)
tab_controls.add(tab2, text = "Tab 2")

#Make the menu visible using pack
tab_controls.pack(expand=True, fill="both")

#Create container frame to hold all other widgets i.e elements using Label frame
weather_condition_frame = ttk.LabelFrame(tab1, text="Current weather conditions")

#tkinter grid layout manager
weather_condition_frame.grid(column=0, row=0, padx=8, pady=4)

# but frame won't be visible until we add widgets to it...
# Adding a Label
ttk.Label(weather_condition_frame, text="Location:").grid(column=0, row=0, sticky='W') # Stick to (W)est boundary

#Start GUI
win.mainloop()
