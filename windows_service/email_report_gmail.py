#! /usr/bin/env python3

"""
Created on Feb 10, 2019
@author: Navedanjum Ansari
"""

# NOTE: In Your gmail account 'Allow access for less secure apps' turned ON
# https://www.google.com/settings/security/lesssecureapps

import smtplib
from email.mime.text import MIMEText
from datetime import datetime
from gmail_passwd import PASSWD


def send_gmail(msg_file):
    with open(msg_file,mode='rb') as message:    #Open (r)ead,(b)inary mode to read html file
        msg = MIMEText(message.read(), 'html' , 'html')  #create html message

        #Create MIME text Subject from and To , field of the email
        msg['Subject'] = 'Hourly Weather Report {}'.format(datetime.now().strftime('%Y-%m-%d %H:%M'))
        msg['From'] = 'navedanjum.ansari@gmail.com'  #replace your logged in gmail/email address
        msg['To']  = 'navedanjum.ansari@gmail.com, navedanjum.ansari1@gmail.com'  #Comma separated multiple addresses to sent  email

        #create smtp server
        server = smtplib.SMTP('smtp.gmail.com',port=587)
        server.ehlo()
        server.starttls()  # Put connection on TLS mode
        server.ehlo()

        server.login('navedanjum.ansari@gmail.com', PASSWD)
        server.send_message(msg)
        server.close()

#===========================================
if __name__ == '__main__':
    from windows_service.Get_weather_data import get_weather_data
    from windows_service.create_html_file import create_html_report
    weather_data, icon = get_weather_data('KLAX')
    email_report = "weather_file.html"
    create_html_report(weather_data,icon, email_report)
    send_gmail(email_report)



