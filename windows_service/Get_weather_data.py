#! /usr/bin/env python3

"""
Created on Feb 10, 2019
@author: Navedanjum Ansari
"""

## NOAA weather data from web : http://www.weather.gov/xml/current_obs/{STATION_ID}.xml

## retrieve tags of our interest
## create an empty dictionary with keys as our tags data of our interest

weather_data_tags_dict = {
    'observation_time': '',
    'weather': '',
    'temp_f': '',
    'temp_c': '',
    'dewpoint_f': '',
    'dewpoint_c': '',
    'relative_humidity': '',
    'wind_string': '',
    'visibility_mi': '',
    'pressure_string': '',
    'pressure_in': '',
    'location': ''
}

## Scrape XML data from the website into the dictionary by parsing xml tags.
import urllib.request

def get_weather_data(station_id = 'KLAX'):
    url_general = 'http://www.weather.gov/xml/current_obs/{}.xml'
    url = url_general.format(station_id)
    request = urllib.request.urlopen(url)
    content = request.read().decode()

    #Use ElemenTree to retrieve specific from xml content
    import xml.etree.ElementTree as ET
    xml_root = ET.fromstring(content)

    #Loop through data dictionary to find matching tags in xml content.

    for data_point in weather_data_tags_dict.keys():
        weather_data_tags_dict[data_point] = xml_root.find(data_point).text

    #Construct the weather icon url from tags url_icon_base and url_icon_name
    url_icon_base = xml_root.find('icon_url_base').text
    url_icon_name = xml_root.find('icon_url_name').text
    url_icon = url_icon_base + url_icon_name

    return weather_data_tags_dict, url_icon

#####################################################
if __name__ == '__main__':
    weather_dict, icon = get_weather_data()
    from pprint import pprint
    pprint(weather_dict)
    pprint(icon)
