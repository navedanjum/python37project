#! /usr/bin/env python3

"""
Created on Feb 10, 2019
@author: Navedanjum Ansari
"""

from Get_weather_data import get_weather_data
from create_html_file import create_html_report

from email_report_gmail import send_gmail
from collections import OrderedDict
from time import sleep
from pprint import pprint
import schedule

def job():
    pprint(schedule.jobs)
    weather_dict, icon = get_weather_data('KLAX')
    weather_dict_ordered = OrderedDict(sorted(weather_dict.items()))

    email_report = "weather_file.html"
    create_html_report(weather_dict_ordered,icon,email_report)
    send_gmail(email_report)

#Schedule every 2 minutes
schedule.every(1).minutes.do(job)

while True:
    schedule.run_pending()
    sleep(1)






