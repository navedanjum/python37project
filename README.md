# Python Project
1. Design patterns in Python
2. Factory, Builder, Abstract Factory
3. Parsing XML, HTML, JSON Data, Reading image 
4. Building GUI Weather App using python Tkinter
5. Creating HTML report, Scheduling as email and converting process into windows service

## Description
* The Python codes are written in Python 3.7.2
* IDE used is Pycharm

## About author
* Navedanjum Ansari
* Linkedin: https://www.linkedin.com/in/navedanjum-a-6018783b/    


## Reference
* Starting windows service: https://stackoverflow.com/questions/41200068/python-windows-service-error-starting-service-the-service-did-not-respond-to-t  
