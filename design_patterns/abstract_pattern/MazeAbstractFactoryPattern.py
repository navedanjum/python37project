from enum import Enum

class MapSite:
    def enter(self):
        raise NotImplementedError('Abstract Base Class method')
    
class Direction(Enum):
    NORTH = 0
    EAST  = 1
    SOUTH = 2
    WEST  = 3
    
class Room(MapSite):
    def __init__(self, roomNo):
        self._sides = [MapSite] * 4
        self._roomNumber = int(roomNo)
        
    def get_side(self, Direction):
        return self._sides[Direction]
    
    def set_side(self, Direction, MapSite):
        self._sides[Direction] = MapSite
        
    def enter(self):
        print('    You have entered room: ' + str(self._roomNumber))
        
class Wall(MapSite):
    def enter(self):
        print('    * You just ran into a Wall...')    

class Door(MapSite):
    def __init__(self, room1=None, room2=None):
        self._room1 = room1
        self._room2 = room2
        self._isOpen = False
        
    def other_side_from(self, room):
        print('\tDoor obj: This door is a side of Room: {}'.format(room._roomNumber))
        if 1 == room._roomNumber:
            other_room = self._room2
        else: 
            other_room = self._room1        
        return other_room
        
    def enter(self):
        if self._isOpen: print('    **** You have passed through this door...')
        else: print('    ** This door needs to be opened before you can pass through it...')

class Maze:
    def __init__(self):
        # dictionary to hold room_number, room_obj <key, value> pairs
        self._rooms = {}
    
    def add_room(self, room):
        # use roomNumber as lookup value to retrieve room object
        self._rooms[room._roomNumber] = room    
    
    def room_no(self, room_number):
        return self._rooms[room_number]
    
    
class MazeFactory:
    @classmethod            # decorator
    def MakeMaze(cls):      # cls, not self
        return Maze()       # return Maze instance 
    
    @classmethod            # decorator
    def MakeWall(cls):
        return Wall()
    
    @classmethod            # decorator
    def MakeRoom(cls, n):   # n = roomNumber
        return Room(n)
        
    @classmethod            # decorator
    def MakeDoor(cls, r1, r2):
        return Door(r1, r2)
    
    
class MazeGame:
    # Abstract Factory
    def CreateMaze(self, factory=MazeFactory):
        aMaze = factory.MakeMaze()
        r1 = factory.MakeRoom(1)
        r2 = factory.MakeRoom(2)
        aDoor = factory.MakeDoor(r1, r2)
        
        aMaze.add_room(r1)
        aMaze.add_room(r2)
        
        r1.set_side(Direction.NORTH.value, factory.MakeWall())
        r1.set_side(Direction.EAST.value, aDoor)
        r1.set_side(Direction.SOUTH.value, factory.MakeWall())
        r1.set_side(Direction.WEST.value, factory.MakeWall())
        
        r2.set_side(Direction(0).value, factory.MakeWall())
        r2.set_side(Direction(1).value, factory.MakeWall())
        r2.set_side(Direction(2).value, factory.MakeWall())
        r2.set_side(Direction(3).value, aDoor)
        
        return aMaze
    
#==================================================================
# Self-testing section    
#==================================================================
if __name__ == '__main__':
    # common code moved into function
    def find_maze_rooms(maze_obj):      # pass object into function
        # find its rooms
        maze_rooms = []
        for room_number in range(5):
            try: 
                # get the room number
                room = maze_obj.room_no(room_number)
                print('\n^^^ Maze has room: {}'.format(room_number, room))
                print('    Entering the room...')
                room.enter()
                # append rooms to list
                maze_rooms.append(room)
                for idx in range(4):
                    side = room.get_side(idx)
                    side_str = str(side.__class__).replace("<class '__main__.", "").replace("'>", "")  
                    print('    Room: {}, {:<15s}, Type: {}'.format(room_number, Direction(idx), side_str))
                    print('    Trying to enter: ', Direction(idx))
                    side.enter()
                    if 'Door' in side_str:
                        door = side                    
                        if not door._isOpen:
                            print('    *** Opening the door...')
                            door._isOpen = True
                            door.enter()
                        print('\t', door)                    
                        # get the room on the other side of the door
                        other_room = door.other_side_from(room)
                        print('\tOn the other side of the door is Room: {}\n'.format(other_room._roomNumber))                    
                
            except KeyError:
                print('No room:', room_number)
        num_of_rooms = len(maze_rooms)
        print('\nThere are {} rooms in the Maze.'.format(num_of_rooms))       
        print('Both doors are the same object and they are on the East and West side of the two rooms.')

    ##########################################################################################################
    print('*' * 21)
    print('*** The Maze Game ***')
    print('*' * 21)
    
    # creating the original Maze passing it in as a Factory
    factory = MazeFactory       # pass in class directly
#     factory = MazeFactory()     # pass in instance of class
    print(factory)
    
    maze_obj = MazeGame().CreateMaze(factory)
    find_maze_rooms(maze_obj)