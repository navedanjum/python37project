#! /usr/bin/env python3
from enum import Enum


class MapSite:
    def enter(self):
        raise NotImplementedError('Abstract base class method')


class Direction (Enum):
    NORTH = 0
    EAST = 1
    SOUTH = 2
    WEST = 3


class Room(MapSite):
    def __init__(self, room_no):
        self._sides = [MapSite] * 4
        self._roomNumber = int(room_no)

    def get_side(self, direction):
        return self._sides[direction]

    def set_side(self, direction, map_site):
        self._sides[direction] = map_site

    def enter(self):
        print('You have entered room: ' + str (self._roomNumber))


class Wall(MapSite):
    def enter(self):
        print('You ran into a wall ..........')


class Door(MapSite):
    def __init__(self, room1=None, room2=None):
        self._room1 = room1
        self._room2 = room2
        self._isOpen = False

    def other_side_from(self, room):
        print('\tDoor obj: This door is a side of Room: {}'.format (room._roomNumber))
        if 1 == room._roomNumber:
            other_room = self._room2
        else:
            other_room = self._room1
        return other_room

    def enter(self):
        if self._isOpen:
            print('You have passed through this door...')
        else:
            print('This door needs to be opened before you can pass through it...')


# Create a Maze with all components maze is in place
class Maze:
    def __init__(self):
        # dictionary to hold room_number, room_obj <key, value> pairs
        self._rooms = {}

    def add_room(self, room):
        # use roomNumber as lookup value to retrieve room object
        self._rooms[room._roomNumber] = room

    def room_no(self, room_number):
        return self._rooms[room_number]


class MazeGame:
    def create_maze(self):
        amaze = Maze ()
        r1 = Room(1)
        r2 = Room(2)
        the_door = Door(r1, r2)

        amaze.add_room(r1)
        amaze.add_room(r2)

        r1.set_side(Direction.NORTH.value, Wall ())
        r1.set_side(Direction.EAST.value, the_door)
        r1.set_side(Direction.SOUTH.value, Wall ())
        r1.set_side(Direction.WEST.value, Wall ())

        r2.set_side(Direction (0).value, Wall ())
        r2.set_side(Direction (1).value, Wall ())
        r2.set_side(Direction (2).value, Wall ())
        r2.set_side(Direction (3).value, the_door)

        return amaze


#################################################
# Test section
#################################################

if __name__ == '__main__':
    #    map_site_inst = MapSite()
    #    map_site_inst.enter()

    print('*' * 25)
    print('*** The Maze Game ***')
    print('*' * 25)

    # create the Maze
    maze_obj = MazeGame().create_maze ()

    # find its rooms
    maze_rooms = []

    for room_number in range (5):
        try:
            # get the room number
            room = maze_obj.room_no (room_number)
            print('\n^^^ Maze has room: {}'.format (room_number, room))
            print('    Entering the room...')
            room.enter ()
            # append rooms to list
            maze_rooms.append (room)
            for idx in range (4):
                side = room.get_side (idx)
                print(side.__class__) # <class '__main__.Wall>
                side_str = str(side.__class__).replace("<class '__main__.", "").replace("'>", "")
                print('    Room: {}, {:<15s}, Type: {}'.format (room_number, Direction (idx), side_str))
                print('    Trying to enter: ', Direction (idx))
                side.enter ()
                if 'Door' in side_str:
                    door = side
                    if not door._isOpen:
                        print('    *** Opening the door...')
                        door._isOpen = True
                        door.enter ()
                    print ('\t', door)
                    # get the room on the other side of the door
                    other_room = door.other_side_from(room)
                    print ('\tOn the other side of the door is Room: {}\n'.format(other_room._roomNumber))

        except KeyError:
            print ('No room:', room_number)
        num_of_rooms = len (maze_rooms)
        print ('\nThere are {} rooms in the Maze.'.format (num_of_rooms))
        print ('Both doors are the same object and they are on the East and West side of the two rooms.')